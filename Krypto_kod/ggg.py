import random
import numpy as np
from decimal import *
import math

_1_50 = 1 << 50  # 2**50 == 1,125,899,906,842,624

def isqrt(x):
    """Return the integer part of the square root of x, even for very
    large integer values."""
    if x < 0:
        raise ValueError('square root not defined for negative numbers')
    if x < _1_50:
        return int(math.sqrt(x))  # use math's sqrt() for small parameters
    n = int(x)
    if n <= 1:
        return n  # handle sqrt(0)==0, sqrt(1)==1
    # Make a high initial estimate of the result (a little lower is slower!!!)
    r = 1 << ((n.bit_length() + 1) >> 1)
    while True:
        newr = (r + n // r) >> 1  # next estimate by Newton-Raphson
        if newr >= r:
            return r
        r = newr

a = open("a", "r")
f = a.readline()
g = bytes(f, 'utf-8')
#print(g)
liczba =int.from_bytes(g,"little")

print (liczba)

getcontext().prec = 1500
liczba2= Decimal(liczba)
pierw = liczba2.sqrt()
iter = Decimal(pierw)
p = Decimal(0)

#iter = iter.__ceil__() + Decimal(1<<127)
iter2 = Decimal(1 << 127)
k = 0
while True:

    potega = iter2 * iter2
    p = Decimal(potega + liczba2)
    p = p.sqrt()
    if (p.__ceil__() ==p and p!=0):
        break
    else:
        iter2 += 1
    k+=1
    if k% 1000000000000==0:
        print("K")

    if (iter2 > (1<<129) ):
        print("Klapa")
        break


print("Wynik")
print(p)
print(iter)


