import decimal
from decimal import Decimal
import numpy as np

decimal.getcontext().prec = 1000


def duze_modulo(a, mod):

    if a <0:
        a = ujemne_modulo(a,mod)

    if a > mod:
        b = a//mod
        a = a - (b * mod)

    return Decimal(a)


def ujemne_modulo(a,mod):
    while a <0:
        a +=mod

    return a



def duze_dzielenie_calkowite(a, b):
    return Decimal(a//b)


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def odwotne_modulo(a, b, napisy):
    a = Decimal(a)
    b = Decimal(b)
    wynik=0
    if a <0:
        a=ujemne_modulo(a,b)
    if a > b:
        a = duze_modulo(a, b)
    try:
        kol1 = [b, a]
        kol2 = [0, 1]
        kol3 =  duze_dzielenie_calkowite(kol1[0], kol1[1])

        if napisy ==1:
            print ( kol1[1],"\t\t",kol2[1],"\t\t",kol3)
        while kol1[1] != 1:

            temp = np.copy(kol1[1])

            # Pierwsza kolumna
            kol1[1] = duze_modulo(kol1[0], kol1[1])
            kol1[0] = temp

            #druga kolumna
            temp = np.copy(kol2[1])

            kol2[1] = kol2[0] - ( kol2[1] * kol3 )
            kol2[0] = temp
            # trzecia kolumna
            if napisy == 1:
                print(kol1[1], "\t\t", kol2[1], "\t\t", kol3)
            kol3 = duze_dzielenie_calkowite(kol1[0], kol1[1])


        wynik = kol2[1]
        if kol2[1] < 0:
            wynik = b + kol2[1]
    except:
        wynik = modinv(a,b)

    return wynik


def El_gamal(p,a,b,k,liczba_losowa, m ):
    p = Decimal(p)
    a = Decimal(a)
    b = Decimal(b)
    k = Decimal(k)
    m = Decimal(m)
    #b = duze_modulo(a**k,p)
    liczba_losowa = Decimal(liczba_losowa)

    Mk1= duze_modulo(a**liczba_losowa,p)
    Mk2 = duze_modulo(m * duze_modulo(b**liczba_losowa,p), p)

    return Mk1, Mk2


def El_gamal_deszyfruj(p,a,Mk1,Mk2 ):
    p = Decimal(p)
    a = Decimal(a)
    Mk1 = Decimal(Mk1)
    Mk2 = Decimal(Mk2)


    return  duze_modulo(Mk1 * Mk2**(p-1-a), p )


def oblicz_rzad(liczba_ciala,wsp1,wsp2):

    do_ilu = liczba_ciala//2
    LICZEBNOSC = {}
    perfekt_kwadraty={}
    for i in range(do_ilu):
        perfekt_kwadraty[duze_modulo((i+1)**2,liczba_ciala)] = i

    licznik = 0
    for i in range(liczba_ciala):
        rownanie = duze_modulo(i**3 + i* wsp1 + wsp2, liczba_ciala)
        a = perfekt_kwadraty.get(rownanie)
        if rownanie == 0:
            licznik += 1
            temp = LICZEBNOSC.get(rownanie)
            if temp is None:
                LICZEBNOSC[rownanie]= 1
            else:
                LICZEBNOSC[rownanie] = 1 + temp
            continue

        if a is not None:
            temp = LICZEBNOSC.get(rownanie)
            if temp is None:
                LICZEBNOSC[rownanie]= 2
            else:
                LICZEBNOSC[rownanie] = 2 + temp
            licznik += 2


    return licznik + 1, LICZEBNOSC


# x^3 + ax +b =0
# 4a^3 + 27b^2 !=0
def pomnoz_krzywe(dziedzina,y1,x1,y2,x2,a,b,tryb):
    Xw=0
    Yw=0


    if tryb == 0:
        temp1 = Decimal(x2-x1)
        temp2 =  odwotne_modulo(y2-y1,dziedzina, 0)
        s = duze_modulo(temp2 * temp1, dziedzina)
        s2 = duze_modulo(s*s, dziedzina)

        Xw = duze_modulo(s2 - (y1+y2),dziedzina)
        Yw = duze_modulo(duze_modulo(s*(y2-Xw),dziedzina) - x2, dziedzina)
    else:

        temp1 = duze_modulo(3 * y1**2  + a, dziedzina)
        temp2 = 2 * x1
        s = duze_modulo( temp1* odwotne_modulo(temp2,dziedzina,0),dziedzina)

        Xw= duze_modulo(s**2 - 2*y1, dziedzina)
        Yw = duze_modulo(s * (y1 - Xw )- x1, dziedzina)

    return Xw,Yw