
import numpy as np
from math import gcd  # LCM

import Krypto.Modalna as moda
import Krypto.Modalna2 as modularna2
import  Krypto.Szyfry as szyfr
import  Krypto.Permutacje as permutacja
import primefac
import os
from Crypto.PublicKey import RSA
from Crypto.PublicKey
private = RSA.generate(1024, os.urandom)


litery = ['A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O',
           'Ó', 'P', 'Q', 'R', 'S', 'Ś', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Ź', 'Ż']





wynik = modularna2.odwotne_modulo(21345678785945339589576896789678967896789678967897869976894,948594856789989889 , 0)
wynik = modularna2.odwotne_modulo(150,233, 0)
#print (wynik)

# (p,a,b,k,jakas_liczba, wiadomosc) , B = a^k mod p
xx = modularna2.El_gamal(97, 15, 57, 17, 8, 13)
yy = modularna2.El_gamal_deszyfruj(97, 15, xx[0], xx[1])
#print(xx)
#print(yy)

zz, liczba = modularna2.oblicz_rzad(47, 1,8)
print(zz)


# mod, x1,y1,x2,y2,a,b,tryb
#qq = modularna2.pomnoz_krzywe(17,5,1,13,9,7,2,1)
#print(qq)

# 8 , 24


p1 = 11
p2 = 9
wspa = 1
wspb = 8
rzad = 47
print("Krok1:    ",p1," ",p2)
qq = modularna2.pomnoz_krzywe(rzad, p1,p2, p1, p2, wspa, wspb, 1)
qq_start = [p1,p2]
print("krok:2","  ",qq)

iterator = 3
for i in range(50):
    try:
        qq = modularna2.pomnoz_krzywe(rzad, qq[0], qq[1], p1, p2, wspa, wspb, 0)
        print("krok:",iterator,"  ",qq)
        iterator +=1
    except:
        print("INF krok:", iterator, "  ", [0,0])
        qq= qq_start
        iterator += 1
        qq = modularna2.pomnoz_krzywe(rzad, qq[0], qq[1], p1, p2, wspa, wspb, 1)
        print("krok:",iterator,"  ",qq)
        iterator +=1







d1 = { 1:4,2:2,3:1, 4:3 , 5:5}
d2 = { 1:4,2:2,4:3,3:1,5:5}
print(permutacja.pomnoz(d1,d2))
print(permutacja.rank_naive(d1))

print(moda.liczba_dzielnikow(9))
# tekst = "PERMUTACJA TOŻSAMOŚCIOWA"
#
#
# for i in range(3):
#     tekst = szyfr.cesar(tekst)
#
# print(tekst)
#
# a = 2**15 -1
# b = 2**17 +1
#
# i = 0
#
# tab=[]
# while i< 1023:
#
#     if (3*i) % 1024 == 1:
#
#         tab.append(i)
#     #print(i)
#     i+=1
#
#
# print(tab)
# print(i)
#
#
# i=0
# while i<10000000000000:
#     if 2**152**17
#
# x= moda.czy_pierwsza(a)
# y =moda.czy_pierwsza(b)

